---
layout: markdown_page
title: "Underperformance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

We want people to be successful and should give every opportunity for individuals to work effectively.
Important deliverables or being understaffed is not a good reason to keep a team member who is underperforming.
We owe it to all of those on the team to maintain a high standard of performance amongst all teammates.
In addition, we also want our teammates to be successful, and recognize that they may be more successful at another company.

In all cases, we want a manager who asks themselves the question "Is this the best person I could hire today?" to respond with a "yes".

## Training video

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/nRJHvzXwXBU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Discuss circumstance immediately with your manager

If there are circumstances in your life that cause you to be less effective immediately tell your manager.
It isn't required to give details if you prefer not to.
Tell your manager when it started, to what extend it hinders your work, and what your forecast is for it to get better.
When you delay this conversation until your manager identifies underperformance you've lost trust that would be helpful to get through this period.

## Identify and take action as early as possible
Taking action sooner allows the action to be less severe and allows more time for coaching to have an effect.
The important thing to remember is to always address any early signs of underperformance immediately.

## Discuss immediately with the manager's manager

Underperformance is between the report, the manager, and their manager.  That is because the manager of the manager needs to see that underperformance is identified early and can help advising proportional actions to address it.
Taking early action on underperformance is one of the most important ways to get results and an essential manager skill.
In order to take the best action and show you are handling it please inform you manager immediately when you've identified possible underperformance.
If your manager warns you about possible underperformance before you tell them you've not practiced [always tell us the bad news promptly](/handbook/leadership/#management-group), this diminishes the trust that is needed to resolve the situation.
Since possible underperformance is an important topic to talk about as soon as possible it is the first item of the [1-1 agenda](/handbook/leadership/1-1/suggested-agenda-format).
Managing underperformance is very important because it sets the standard for the performance we accept.
It is hard because frequently the underperformance is due to a mistake we made in hiring, on boarding and/or coaching.
It is also hard because it is a hard message to give and receive that someone's performance is not adequate.

## Options for remediation

1. Coaching
1. Letting someone go
1. Performance Improvement Plan (PIP)

## Coaching

Coaching is the preferred option to deal with underperformance.

Managers are expected to address performance concerns (poor results and/or behavior issues) in a timely manner.  Managers should address concerns verbally during one-on-one meetings or in impromptu private coaching sessions with their team member. These conversations should be documented by the manager and shared with the team member so that everyone has a record of the discussion and is in alignment on where improvements needs to be made and by when. Documentation should be brief (a few key bullet points or a paragraph), and should be shared with the team member within 24 hours of the verbal discussion.

Helping GitLab team-members understand clearly how their performance is below the standard expected quickly is very important to foster immediate improvement and continued success. It is also important to clarify when feedback given can provide helpful coaching vs. when to address a serious performance issue. It is not always clear how serious the feedback being provided is and setting the context can be critical.
If there are extenuating circumstances, some leeway may be granted depending on the situation. This is an area where a People Business Partner can provide a sounding board or voice of reason.

When underperformance is detected, managers compensate by checking their report's work more frequently. The company results should not be affected more than through under-hiring where the position was vacant and others would step in to compensate.

## Letting someone go

This should be discussed with a People Business Partner before any action is taken in order to ensure it is done in compliance with local laws and regulation. As soon as you know you'll have to let someone go, do it immediately. The team member is entitled to know where they stand. Delaying it for days or weeks causes problems with confidentiality (finding out that they will be let go), causation (attributing it to another reason), and escalation (the working relationship is probably going downhill).

## Performance Improvement Plan (PIP)

### Not a first step to letting someone go

Many companies use a PIP for most involuntary terminations, as documented support for releasing a team member.
At GitLab we think that giving someone a plan while you intend to let them go is a bad experience for the team member, their manager, and the rest of the team.
A PIP at GitLab is not used to "check the box" a PIP is a genuine last chance to resolve underperformance.
You should only offer a PIP if you are confident that the team member can successfully complete it. The team member should also be committed to successfully completing the PIP and maintaining the level of performance arrived at through the PIP.  A PIP will not be successful unless the team member and the manager believes they can succeed.

### Unlikely for directors and up

For director and higher functions we are unlikely to offer a PIP and more likely to let someone go immediately after coaching didn't help. This since the impact of their underperformed is greater on the rest of the organization and it takes more time to asses an improvement in performance.

### SMART Goals
As part of the PIP the manager will work with the team member to define SMART goals.  SMART goals allow both the manager and team member to define requirements, track progress, and improve communication of expectations for success during the PIP period.

**SMART** is an acronym that can be used in creating the PIP requirements. To make sure goals are clear and reachable, each one should be:

* **Specific:**  Specifically define what you expect the employee to do/deliver.  Avoid generalities and use action verbs as much as possible.
* **Measurable:** You should be able to measure whether the employee is meeting the goals or not.  Identify how you will measure success - usually stated in terms of quantity, quality, timeliness or cost (e.g. increase by 25%).
* **Achievable:** Make sure that accomplishing the goal is within the employee's realm of authority and capabilities.  While considering whether a goal is actionable/achievable, you also need to consider the employee's total set of goals.  While each individual goal may be achievable, overall, you may be assigning the employee more goals than they can reasonably be expected to successfully complete.
* **Realistic:** Can the employee realistically achieve the objectives with the resources available?  Ensure the goal is practical, results-oriented and within the employees realm of authority and capabilities.  Also, Relevant:  Where appropriate, link the goal to a higher-level departmental or organizational goal, and ensure that the employee understands how their goal and actions contributes to the attainment of the higher level goal.  This give the employee a context for their work.
* **Time-bound:** When does the objective need to be completed?  Specify when the goal needs to be completed (e.g. by the end of the quarter, or every month).

**Sample SMART Goals:**

Bad SMART Goal: "Improve overall qualified sales lead".

Good SMART Goal: "In May, June and July, Jane Doe must have an increase of 20% in overall qualified sales leads entered into Salesforce.com"

Bad SMART Goal: "Increase Fix defects"

Good SMART Goal: "Fix at least 8 defects. Must be fixed with code changes, not closing as "Won't Fix, "Not Reproducible".  All defects must be dev completed/merged by end of business Monday, Jan. 1st, 2018".

#### CREDIT Values applied to the PIP 

Our values should be top of mind in administering a PIP. 

* **Collaboration**: The PIP is an opportunity for manager and the team member to work together towards a desired outcome. Pay attention to the timeline and collaborate on next steps after each milestone is achieved.
* **Results**: The goal of the PIP is to see results, that could be an improvement in performance and/or a changed behavior. Did productivity increase? Are assigned projects being completed on time? Is there an increase in the Sales pipeline? Those results must also be attainable and measurable. 
* **Efficiency**: The manager provides the support needed for the team member to achieve the required results but the ownership and accountability lies with the team member to drive their own performance. The PIP ensures that the team members will correct specific issues identified in a timely manner, under a managed process, showing specific results. Timely manner is dependent on the timeline agreed to by both the Manager and the team member. Typically, a PIP could last 2-4 weeks depending on the role and circumstances.
* **Diversity**: As part of our efforts to foster an environment where everyone can thrive, the PIP should be viewed as a valuable tool designed to address individual behavior. Since each team member is unique, Managers should be mindful that they continue to acknowledge and adjust for diversity of thoughts, differences in communication style and learning preference while following the PIP.
* **Iteration**: Change is expected during the PIP. So small and consistent changes, or iterations, that move the team member toward the final required change should be expected. Positive, effective changes that continue after the PIP should also be encouraged.  Improvement must be maintained.
* **Transparency**: Have regular and honest communication during the PIP. As a manager, set expectations around communication upfront. As a team member, share challenges as they come up. If things are not going well and either party wishes to end the PIP early, be open about that and discuss a mutual separation and any related severance.

### Experience

It is important to remember that the root cause of issues can be a variety of things, PIPs are not intended to be a negative document. They are an opportunity for the manager and the team member to work together to get the team member back on track. We have an example of this to share here, it is anonymized in line with keeping job feedback private as per the [General Guidelines](/handbook/general-guidelines);

**GitLab team-member:**

"Although nobody wants to be put on a PIP, for me it ended up ultimately being a positive experience.  My initial introduction to the plan was a shock and a serious blow to my self confidence, but the process was presented in a fair and open way with clearly defined steps and goals.  The document presented an attitude of wanting to help me improve and thrive, not a pretext to send me out the door.  This helped me shape my attitude going through the process.  As it turns out I had several blind spots in my communication and time management skills that needed to be remedied, and over the course of the PIP with weekly updates with my manager and some personal efforts in activity logging I was able to improve in both of these areas".

**Manager:**

"For me as a manager, I want to be honest and open with people. I never feel good about telling people they are not meeting the standard. At the same time I really want people to improve. With the PIP we were able to clearly talk about the work that needed to be done to, make them improve and get them where we needed them to be. In this case, the underperformance was not a lack of skills. We merely needed to redirect their focus".

### Process

The intention of a PIP is to support the team member in any way required to make their time at GitLab a positive experience but also to make clear that immediate and sustained improvement
is required. The Society for Human Resources Management (SHRM) has a [helpful guide](https://www.shrm.org/templatestools/howtoguides/pages/performanceimprovementplan.aspx) to review when
you this step is needed to push past the current performance issues.

A performance improvement plan includes the following:

   * Evaluation of current work
   * Clear direction including metrics and concrete goals to improve (e.g. finish X before Y)
   * Resources/coaching necessary to achieve goals

Here is a [basic PIP template](https://docs.google.com/document/d/1AsVwUikcUofl58eLWhiEEUFJqtwgUQNdDo5lM98bP7o/edit) which will provide a good start to creating the document. For an alternative format, you can use this [alternative template](https://docs.google.com/document/d/1c1LGzd83nvXU-JcknuXO72lQjHROPykWVglfPZpZ6kY/edit) Whichever template you choose, it should be customized to fit the particular situation. All PIPs should be forwarded to the People Business Partner for final review and approval before delivery. This step will help ensure consistency in the PIP process for any affected team member and to protect GitLab should legal claims arise as a result of termination.

3) Team member gets time (2-4 weeks depending on the role and circumstances) to demonstrate improvement and meet specific goals outlined in the PIP. If sufficient improvement is not made but progress is headed in the right direction, a plan period may be extended at the discretion of the manager.  By design, a PIP is expected to support a successful and sustained improvement in performance.

4) Otherwise, the team member is let go or their contract is cancelled. It is not necessary to create a second PIP for the same performance issues within a reasonable period of time and after informing the team member that the unacceptable performance has resurfaced in writing (an email is fine). To begin the termination process, the manager should forward a recommendation for termination to their Executive team member and People Business Partner including the history of the PIP and the recurring performance issues. If a team member does need to be let go, work with HR Business Partner to follow the process for [involuntary termination](/handbook/people-operations/#involuntary-terminations) and the [offboarding steps](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/offboarding.md).

### How to deliver a PIP

 - Once the [PIP is documented](https://docs.google.com/document/d/1AsVwUikcUofl58eLWhiEEUFJqtwgUQNdDo5lM98bP7o/edit) schedule a call with the team member outside of the regular 1-1
 - Present the PIP to them on the call and go through it with them. Read it verbatim and don't deviate from it or think of other things to add during the call
 - Re-iterate that you want them to improve and you're there to support them. You will be checking their progress each week (or day if really necessary)
 - Ask them if they have any questions
 - Inform the People Business Partner that the PIP conversation has taken place and the document have been sent to the team member.
 - The People Ops Business Partner will upload the PIP document to the team members BambooHR.  The PIP will be stored on the Documents page in the Performance Reviews folder. 

### Confidential

The PIP process is between a manager and their direct report. Information shared in a PIP should be kept confidential by both participants. If underperformance becomes a reason for termination, the individual should not be taken by surprise but the rest of the company should be.
