---
layout: markdown_page
title: "BC.1.01 - Business Continuity Plan Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# BC.1.01 - Business Continuity Plan

## Control Statement

GitLab's business contingency plan is reviewed, approved by management and communicated to relevant team members biannually.

## Context

The review cycle for business continuity plans are designed to ensure all information in the plan is as up-to-date as possible. A business continuity plan is only effective if users can trust the accuracy of the information in the plan. The business continuity plan is meant to be a comprehensive runbook that can walk all GitLab team-members through exactly what their individual responsibilities are in the event of a disruption to GitLab operations. This triggering event can be anything from a malicious breach of our systems to a global datacenter disruption.

## Scope

The business continuity plan is comprehensive by nature and will impact all GitLab stakeholders.
The scope of a Business Continuity Plan can be categorized into the following seven steps:

Identify the critical business functions
Identify the critical systems & its dependencies
Identify the risks to business
Specify and confirm the data backup and recovery plan are working efficiently.
Clearly document the functions and procedures of the BC plan, who should lead the effort and who are all the players involved
Prepare a detailed communication plan
Test, assess, learn and improve

## Ownership

* Corporate Compliance owns this control.
* Infrastructure group acts as process owners, implementing and providing support for this control.
* Security Compliance acts as a facilitator to ensure this control is followed.

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/BC.1.01_business_continuity_plan.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/BC.1.01_business_continuity_plan.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/BC.1.01_business_continuity_plan.md).

## Framework Mapping

* ISO
  * A.17.1.1
  * A.17.1.2
* SOC2 CC
  * CC7.5
  * CC9.1
* SOC2 Availability
  * A1.2
* PCI
  * 12.10.1
