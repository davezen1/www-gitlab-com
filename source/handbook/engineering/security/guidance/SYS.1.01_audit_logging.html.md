---
layout: markdown_page
title: "SYS.1.01 - Audit Logging Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# SYS.1.01 - Audit Logging

## Control Statement

GitLab logs critical information system activity.

## Context

Logging is the foundation for a variety of other security controls including monitoring, incident response, and configuration management. Without comprehensive and reliable logs, large parts of our security compliance program wouldn't be possible. This control is left vague by design. As we develop our system maps and inventories this control will likely become a bit more targeted. To start we really want all GitLab teams to enable system-level logging on all production systems.

An auditor will look to validate in-scope systems are generating logs, those logs are collected, retained the required amount of time and utilized to monitor for performance, health, and anomalies.  To validate the control is working properly, the auditor should require additional pieces of information to demonstrate audit logging is functioning properly.  Those information items include:

* Review of the audit and accountability policy and procedures
* Confirmation that audit events are reviewed and updated on a recurring basis
* Review what should be collected for auditing and cross-reference against what is collected
* Determine what defines a production system to validate the correct systems are being audited
* Confirm log validation processes are working as intended
* Master asset listing to confirm correct systems are being audited
* Log collection process(es)

## Scope

This logging control applies to all production systems.

## Ownership

Control Owner: 

* Compliance

Process Owners:

* Compliance
* Infrastructure
* Security

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SYS.1.01_audit_logging.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SYS.1.01_audit_logging.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SYS.1.01_audit_logging.md).

## Framework Mapping

* ISO
  * A.12.4.1
* SOC2 CC
  * CC7.2
* NIST 800-53
  * AU-1 
  * AU-2
  * AU-9
  * AU-12
