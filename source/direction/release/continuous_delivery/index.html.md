---
layout: markdown_page
title: "Category Vision - Continuous Delivery"
---

- TOC
{:toc}

## Continuous Delivery

Many teams are reporting that development velocity is stuck; they've
reached a plateau in moving to more and more releases per month, and now
need help on how to improve. According to analyst research, 40% of software development
team's top priorities relate to speed/automation, so our overriding vision
for Continuous Delivery is to help these teams renew their ability to
accelerate delivery.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=continuous%20delivery&sort=milestone)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1294) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

### Continuous Delivery vs. Deployment

We follow the well-known definitions from Martin Fowler on the
difference between continuous delivery and continuous deployment:

- **Continuous Delivery** is a software development discipline
  where you build software in such a way that the software can
  be released to production at any time. You achieve continuous
  delivery by continuously integrating the software done by the
  development team, building executables, and running automated tests
  on those executables to detect problems. Furthermore you push the
  executables into increasingly production-like environments to
  ensure the software will work in production.
- **Continuous Deployment** means that every change goes through the
  pipeline and automatically gets put into production, resulting in many
  production deployments every day. Continuous Delivery just means that
  you are able to do frequent deployments but may choose not to do it,
  usually due to businesses preferring a slower rate of deployment. In
  order to do Continuous Deployment you must be doing Continuous Delivery.

_Source: [https://martinfowler.com/bliki/ContinuousDelivery.html](https://martinfowler.com/bliki/ContinuousDelivery.html)_

### Incremental Rollout

One of the key sub-areas within Continuous Delivery that we're tracking
is incremental rollout. This is important because it enables unprecedented
control over how you deliver your software in a safe way.  For these
items we're also tagging the label "incremental rollout", and you can
see an issue list of these items [here](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=incremental%20rollout&sort=milestone).
Incremental rollout also serves as a key pillar for our [Progressive Delivery](https://about.gitlab.com/direction/cicd/#progressive-delivery)
strategy.

## What's Next & Why

Now that we have delivered [gitlab-ee#7380](https://gitlab.com/gitlab-org/gitlab-ee/issues/7380),
the ability to run a pipeline against the potential merge result, we're able to take the next step
towards merge trains via [gitlab-ee#9186](https://gitlab.com/gitlab-org/gitlab-ee/issues/9186). This
will allow for a merge train to be set up where a series of MRs are queued up, one after the other,
and merged in sequence against the merge result. This will create a very safe, reliable way to deliver
your MRs. 

The one drawback with this first iteration is that it is slow - one at a time can result in long
queues in a lot of different situations, so we plan to immediately follow this up with [gitlab-ee#11222](https://gitlab.com/gitlab-org/gitlab-ee/issues/11222)
which will introduce parallalization to merge trains.

## Maturity Plan

This category is currently at the "Complete" maturity level, and
our next maturity target is Lovable (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:

- [Run pipeline on merge result](https://gitlab.com/gitlab-org/gitlab-ee/issues/7380) (Complete)
- [Parallel execution strategy for Merge Trains](https://gitlab.com/gitlab-org/gitlab-ee/issues/11222) (Complete)
- [Post-deployment monitoring MVC](https://gitlab.com/gitlab-org/gitlab-ee/issues/8295)
- [Deploy button to make initial CI/CD setup easier](https://gitlab.com/gitlab-org/gitlab-ce/issues/52880)
- [More out of the box incremental rollout options](https://gitlab.com/gitlab-org/gitlab-ee/issues/1387)
- [Better guidance for more CD approaches](https://gitlab.com/gitlab-org/gitlab-ce/issues/52875)
- [Actionable CI/CD metrics](https://gitlab.com/gitlab-org/gitlab-ee/issues/7838)

Additionally, we are considering [cloud-native buildpacks](https://gitlab.com/gitlab-org/gitlab-ce/issues/55840)
as the foundation for Continuous Delivery. Depending on how that project
plays out, it may play an important role here - in the meantime we are monitoring
that project for progress.

## Competitive Landscape

Because CI/CD are closely related, the [competitive analysis for Continuous Integration](/direction/verify/continuous_integration#competitive-landscape)
is also relevant here. For how CD compares to other products in the market,
especially as it relates to pipelines themeselves, also take a look there.

As far as CD specifically, has always been strong in providing actionable metrics on
development, and as they move forward to integrate GitHub and Azure
DevOps they will be able to provide a wealth of new metrics to help
teams using their solution. We can stay in front of this by improving
our own actionable delivery metrics in the product via [gitlab-ee#7838](https://gitlab.com/gitlab-org/gitlab-ee/issues/7838).

Spinnaker and Harness are also two modern, cloud-native CD platforms
that provide excellent solutions for delivering to cloud environments
using native approaches like Kubernetes. In order to remain competitive,
we must provide more advanced deployment operator solutions via
[gitlab-ee#1387](https://gitlab.com/gitlab-org/gitlab-ee/issues/1387).
Additionally, these products manage the deployment all the way through
to monitoring, which we will introduce via [gitlab-ee#8295](https://gitlab.com/gitlab-org/gitlab-ee/issues/8295).

## Analyst Landscape

In our conversations with industry analysts, there are a number of key trends
we're seeing happening in the CD space:

### Cloud Adoption
Cloud adoption of CI/CD is growing, with Docker adoption leading the
way and serverless likely next. People need guidance solving CD
because they are stepping out into the dark without mature solutions
already available to them; for example, AWS' current solution for
serverless (functions) CI is just "edit them live." Customers
complained that their products were starting to feel legacy, but
where they go next is unclear.

### Customer Experience
Customer experience is becoming a key metric. Users are looking for
the ability to not just  measure platform stability and other
performance KPIs post-deployment but also want to set targets for
customer behavior, experience, and financial impact. Tracking and
measuring this indicators after deployment solves an important
pain point. In a similar fashion, creating views which managing
products not projects or repos will provide users with a more
relevant set of data.

### Other Topics
In addition to supporting the trends above, there are some key areas
where we can focus that will improve our solution in areas that
analysts are hearing customer demand:

- **Modeling (and autodiscovery/templatizing) of environments and deployments**
  as-code, and in a way that they can automatically interact with each
  other to generate deployment plans without requiring scripting. Views
  that make these complex relationships between environments and
  deployments clear.
- [gitlab-ee#8295](https://gitlab.com/gitlab-org/gitlab-ee/issues/8295): **Deployment status reporting, monitoring behavior, and error handling**,
  including automated rollbacks, incremental rollouts, and other intelligent
  built-in and customizable strategies and behaviors. This can include
  predictive analytics/ML and reporting about successes, failures, and pipeline health,
  both retrospective and runtime. Customizable and tailored for the
  user/role engaging with the data.
- **Configuration drift detection** between what's in our internal model of
  what's deployed, vs. what's actually in the environments with alerting and
  potential automatic remediation.
- [gitlab-ee#11222](https://gitlab.com/gitlab-org/gitlab-ee/issues/11222): Take control of your software delivery by **assembling associated changes into merge trains**
  which can flow through your environments and on to production as a coherent
  group.

## Top Customer Success/Sales Issue(s)

The ability to control pipeline concurrency in an environment (especially
later ones such as production or staging) is frequently mentioned by CS
and in the sales cycle as a feature teams are looking for. This will be
implemented via [gitlab-ce#20481](https://gitlab.com/gitlab-org/gitlab-ce/issues/20481).

## Top Customer Issue(s)

Our most popular customer request is [gitlab-ce#20481](https://gitlab.com/gitlab-org/gitlab-ce/issues/20481),
which limits per-environment pipeline concurrency. This is an important
feature because it allows teams to constrain concurrent deployments, making
things more predictable and/or managing finite resources associated with
environments (for example, a single deploy and test cycle at a time to a
performance testing environment.)

## Top Internal Customer Issue(s)

Finishing merge trains via [gitlab-ee#9186](https://gitlab.com/gitlab-org/gitlab-ee/issues/9186)
will allow our own engineering teams to be more effective while maintaining
a stable, releasable master branch.

### Delivery Team

The Delivery team at GitLab is moving towards CD with https://gitlab.com/gitlab-org/release/framework/issues/1.
That issue is intended to capture items that are important for that effort,
and other use cases and information that can lead into a better CD product
for our users as well as successful migration to CD at GitLab. A design
document is coming from delivery team for the actual process. we can use
this to see how this would be modeled in GitLab; see also the 
[CI/CD Blueprint](https://about.gitlab.com/handbook/engineering/infrastructure/blueprint/ci-cd/)

- [gitlab-ee#9427](https://gitlab.com/gitlab-org/gitlab-ee/issues/9427) helps improve the way we are managing release checklists in issues today
- [gitlab-ce#51738](https://gitlab.com/gitlab-org/gitlab-ce/issues/51738) adds blackout periods
- [gitlab-ee#9186](https://gitlab.com/gitlab-org/gitlab-ee/issues/9186) & [gitlab-ee#7380](https://gitlab.com/gitlab-org/gitlab-ee/issues/7380) enable building on target before merging to keep master green
- [gitlab-ce#57581](https://gitlab.com/gitlab-org/gitlab-ce/issues/57581) sets a maximum commits behind to merge

We are also working to internally adopt some recently released features:

- Releases Page: [framework#141](https://gitlab.com/gitlab-org/release/framework/issues/141)
- Pipelines for Merge Requests: [gitlab-ce#57190](https://gitlab.com/gitlab-org/gitlab-ce/issues/57190)

### SRE Team

From a SRE standpoint, see:

- [gl-infra#52](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/52) takes over management of the CI/CD infrastructure
- [gl-infra#51](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/51) takes over on-call for CI/CD infrastructure

### Multi-team items

- [gitlab-org#762](https://gitlab.com/groups/gitlab-org/-/epics/762) locks down the path to production

## Top Vision Item(s)

Beyond completing the 2018 vision, the most important major step
forward for GitLab CD is [gitlab-ee#8295](https://gitlab.com/gitlab-org/gitlab-ee/issues/8295)
which will introduce post-deployment monitoring, creating a foundation
for advanced deployment features like automated rollbacks and summary
reports of environment behavior before and after the deployment.
