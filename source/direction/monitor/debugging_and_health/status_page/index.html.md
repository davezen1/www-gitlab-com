---
layout: markdown_page
title: "Category Vision - Status Page"
---

- TOC
{:toc}

## Status Page
A common need for many companies is to communicate the status of a software service. This could be to internal users, stakeholders, or the general public.

### Why GitLab?
GitLab is in a unique position to offer compelling value here:
1. We can generate alerts based on a wide array of observability data: metrics, traces, logs, and blackbox monitoring
1. [We are adding comprehensive Incident Management](https://gitlab.com/groups/gitlab-org/-/epics/349)
1. GitLab has a decentralized runner architecture which can run tests from across the world (or via a potential SaaS service) to detect reachability problems

## What's Next & Why
We can start down this road by:
* Build a public page to surface this information cleanly, similar to GitLab Pages but able to deliver live data like charts and incident information
* Integrate with Incident Management, to allow an Ops person to acknowledge and incident and publish status. (If unacknowledged for X minutes, perhaps auto-publish.)
* Once published, make it easy to update directly from the incident.
* Display an availability chart for each service. 
* Depending on demand, consider providing a SaaS service for the status page hosting as well as worldwide blackbox monitoring services

## Maturity Plan
* [Base Epic](https://gitlab.com/groups/gitlab-org/-/epics/208)

## Competitive Landscape
Not yet, but accepting merge requests to this document.

## Analyst Landscape
Not yet, but accepting merge requests to this document.

## Top Customer Success/Sales Issue(s)
Not yet, but accepting merge requests to this document.

## Top Customer Issue(s)
Not yet, but accepting merge requests to this document.

## Top Internal Customer Issue(s)
Not yet, but accepting merge requests to this document.

## Top Vision Item(s)
Not yet, but accepting merge requests to this document.
